## Another look at `Parser` { data-transition="fade none" }

---

## Another look at `Parser`

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
type Input = String

data ParseResult a =
  Error String
  | Success a Input
  deriving Show

newtype Parser a =
  Parser (Input -> ParseResult a)

parse :: Parser a -> Input -> ParseResult a
parse (Parser p) = p
</code></pre>

---

## Another look at `Parser`

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
<mark>type Input = String</mark>

data ParseResult a =
  Error String
  | Success a Input
  deriving Show

newtype Parser a =
  Parser (Input -> ParseResult a)

parse :: Parser a -> Input -> ParseResult a
parse (Parser p) = p
</code></pre>

<div style="font-size: x-large; text-align: center">The input to a parser is a list of character `(String)`</div>

---

## Another look at `Parser`

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
type Input = String

<mark>data ParseResult a =</mark>
  <mark>Error String</mark>
  <mark>| Success a Input</mark>
  <mark>deriving Show</mark>

newtype Parser a =
  Parser (Input -> ParseResult a)

parse :: Parser a -> Input -> ParseResult a
parse (Parser p) = p
</code></pre>

<div style="font-size: x-large; text-align: center">a `(ParseResult a)` is either an error message or (a value of the type `(a)` and *the remaining input*)</div>

---

## Another look at `Parser`

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
type Input = String

data ParseResult a =
  Error String
  | Success a Input
  deriving Show

<mark>newtype Parser a =</mark>
  <mark>Parser (Input -> ParseResult a)</mark>

parse :: Parser a -> Input -> ParseResult a
parse (Parser p) = p
</code></pre>

<div style="font-size: x-large; text-align: center">A parser is a data type that wraps a function from input to a parse result</div>

---

## Another look at `Parser`

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
type Input = String

data ParseResult a =
  Error String
  | Success a Input
  deriving Show

newtype Parser a =
  Parser (Input -> ParseResult a)

<mark>parse :: Parser a -> Input -> ParseResult a</mark>
<mark>parse (Parser p) = p</mark>
</code></pre>

<div style="font-size: x-large; text-align: center">`parse` gets the function out of a `Parser`</div>

---

## Another look at `Parser`

<div style="font-size:28px">
<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
type Input = String

data ParseResult a =
  Error String
  | Success a Input
  deriving Show

newtype Parser a =
  Parser (Input -> ParseResult a)

parse :: Parser a -> Input -> ParseResult a
parse (Parser p) = p
</code></pre>
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- exercise10
instance Functor ParseResult where
</code></pre>

---

## Another look at `Parser`

<div style="font-size:28px">
<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
type Input = String

data ParseResult a =
  Error String
  | Success a Input
  deriving Show

newtype Parser a =
  Parser (Input -> ParseResult a)

parse :: Parser a -> Input -> ParseResult a
parse (Parser p) = p
</code></pre>
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- exercise11
instance Functor Parser where
-- exercise12
instance Applicative Parser where
-- exercise13
instance Monad Parser where
</code></pre>

---

## Another look at `Parser`

<div style="font-size:28px">
<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
type Input = String

data ParseResult a =
  Error String
  | Success a Input
  deriving Show

newtype Parser a =
  Parser (Input -> ParseResult a)

parse :: Parser a -> Input -> ParseResult a
parse (Parser p) = p
</code></pre>
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- exercise14
-- fails a parser with the given error message
failed :: String -> Parser a
-- exercise15
-- parse one character, or fail if no character available
character :: Parser Char
-- exercise16
-- parse one character that satisfies the given function
-- if the character does not satisfy the given function,
--   fail with the given error message
satisfy :: String -> (Char -> Bool) -> Parser Char
</code></pre>

---

## Parser tests

<div style="font-size:18px">
<pre><code class="language-haskell hljs" data-trim data-noescape>
-- test1: Error "hello"
test1 = (+1) &lt;$> Error "hello"
-- test2: Success 100 "bye"
test2 = (+1) &lt;$> Success 99 "bye"
-- test3: Error "message"
test3 = parse (failed "message") "input"
-- test4: Success 'i' "nput"
test4 = parse character "input"
-- test5: Error "EOF"
test5 = parse character ""
-- test6: Success 'i' "nput"
test6 = parse (satisfy "is i" (=='i')) "input"
-- test7: Error "is x"
test7 = parse (satisfy "is x" (=='x')) "input"
-- test8: Error "EOF"
test8 = parse (satisfy "is x" (=='x')) ""
-- test9: Success ('i','n') "put"
test9 =
  parse (
    character >>= \c1 ->
    character >>= \c2 ->
    pure (c1, c2)
  ) "input"
-- test10: Success ('i','n') "put"
test10 =
  parse (
    character >>= \c1 ->
    satisfy "is n" (== 'n') >>= \c2 ->
    pure (c1, c2)
  ) "input"
-- test11: Error "is x"
test11 =
  parse (
    character >>= \c1 ->
    satisfy "is x" (== 'x') >>= \c2 ->
    pure (c1, c2)
  ) "input"
-- test12: Error "EOF"
test12 =
  parse (
    character >>= \c1 ->
    satisfy "is x" (== 'x') >>= \c2 ->
    pure (c1, c2)
  ) "i"
</code></pre>
</div>

