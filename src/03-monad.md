## More Constraints! { data-transition="fade none" }

* All things that are `Applicative` instances are also `Functor` instances
* Can we usefully tighten the constraint further?
* **All things that are `?` instances are also `Applicative` instances**

---

## More Constraints { data-transition="fade none" }

###### ?

<div style="text-align:center">
  <a href="images/superclass-subclass-labelled-03.png">
    <img src="images/superclass-subclass-labelled-03.png" alt="Functor/Applicative/? superclass/subclass relationship" style="width:920px"/>
  </a>
</div>

---

## The general shape of the tighter constraint

###### ?

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmap ::    (a -> b) -> k a -> k b
(&lt;*>) :: k (a -> b) -> k a -> k b
? ::     (a -> k b) -> k a -> k b
</code></pre>

---

## The general shape of the tighter constraint

###### "bind"

<pre><code class="language-haskell hljs" data-trim data-noescape>
(=&lt;&lt;) :: (a -> k b) -> k a -> k b
</code></pre>

---

## The general shape of the tighter constraint

###### "bind" with arguments flipped

<pre><code class="language-haskell hljs" data-trim data-noescape>
(=&lt;&lt;) :: (a -> k b) -> k a -> k b
(>>=) :: k a -> (a -> k b) -> k b
</code></pre>

* The bind operation is often given with the arguments flipped
* We will soon see why

---

## Monad, the tighter constraint { data-transition="fade none" }

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Applicative k => Monad k where
  (>>=) :: k a -> (a -> k b) -> k b
  return :: a -> k a
</code></pre>

---

## Monad, history

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Applicative k => Monad k where
  (>>=) :: k a -> (a -> k b) -> k b
  <mark>return</mark> :: a -> k a
</code></pre>

* `Applicative` was not discovered as a useful intermediate abstraction until long after `Monad`
* Consequently, `Monad` carried the unital operation `(a -> k a)` called `return`
* The operation on `Monad` still exists today, entirely due to *historical reasons*
* It otherwise belongs on `Applicative` `(pure)`

---

## The general shape of Functor/Applicative/Monad

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmap ::    (a -> b) -> k a -> k b
(&lt;*>) :: k (a -> b) -> k a -> k b
(=&lt;&lt;) :: (a -> k b) -> k a -> k b
</code></pre>

---

## The general shape of Functor/Applicative/Monad

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmap ::    (a -> b) -> k a -> k b
(&lt;*>) :: <mark>k</mark> (a -> b) -> k a -> k b
(=&lt;&lt;) :: (a -> <mark>k</mark> b) -> k a -> k b
</code></pre>

---

## Why is `Monad` subclass of `Applicative`? { data-transition="fade none" }

* Using bind (and the unital operation), we can recover apply

<pre><code class="language-haskell hljs" data-trim data-noescape>
(&lt;*>) :: <mark>k</mark> (a -> b) -> k a -> k b
(=&lt;&lt;) :: (a -> <mark>k</mark> b) -> k a -> k b
(>>=) :: k a -> (a -> <mark>k</mark> b) -> k b
</code></pre>

---

## Why is `Monad` subclass of `Applicative`? { data-transition="fade none" }

* Examine this expression
* What is its type?
* Good pen &amp; paper exercise!

<pre><code class="language-haskell hljs" data-trim data-noescape>
\f a ->
  f >>= \ff ->
  a >>= \aa ->
  pure (ff aa)
</code></pre>

---

## Why is `Monad` subclass of `Applicative`? { data-transition="fade none" }

* Keeping in mind:
* `(>>=) :: k a -> (a -> k b) -> k b`
* `pure :: a -> k a`
* Calculate the type

<pre><code class="language-haskell hljs" data-trim data-noescape>
\f a ->
  f >>= \ff ->
  a >>= \aa ->
  pure (ff aa)
</code></pre>

---

## A general intuition for Functor/Applicative/Monad

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- Functor solves problems of this shape
(a -> b) -> k a -> k b
-- Applicative solves problems of this shape
(a -> b -> c) -> k a -> k b -> k c
-- Monad solves problems of this shape
(a -> k b) -> (b -> k c) -> (a -> k c)
</code></pre>

* `Functor` turns `(a -> b)` into `(k a -> k b)`
* `Applicative` does the same as `Functor`, but for functions of any number of arguments
* `Monad` chains functions, where a subsequent function's argument has a **dependency** on a previous return value

---

## Exercises

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- what is the type of this expression?
exercise0 :: ?
exercise0 =
  \f a ->
    f >>= \ff ->
    a >>= \aa ->
    pure (ff aa)
-- write a function of this type
exercise1 ::
  Applicative k => (a -> b -> c) -> k a -> k b -> k c
-- write a function of this type
exercise2 ::
  Monad k => (a -> k b) -> (b -> k c) -> (a -> k c)
</code></pre>

---

## `Monad` instances

* The instances of `Applicative` that we have seen already, also can instance `Monad`
* `instance Monad Optional where`
* `instance Monad List where`
* `instance Monad ((->) t) where`

---

## `Monad` instances, try it!

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Optional a = Empty | Full a
data List a = Nil | Cons a (List a)
data Function a b = Function (a -> b)

instance Monad Optional where -- exercise3
instance Monad List where -- exercise4
instance Monad (Function a) where -- exercise5
</code></pre>

---

## `Monad` laws { data-transition="fade none" }

* Like `Functor` and `Applicative`, the `Monad` type-class has **laws**
* Remember, these laws are *not* enforced by the compiler
* It is a programmer's responsibility to ensure type-class laws are followed

---

## `Monad` laws { data-transition="fade none" }

* All instances must satisfy these laws
* Do your instances meet these laws?

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- law of left identity
forall f x. pure x >>= f <mark>==</mark> f x
-- law of right identity
forall x. x >>= pure <mark>==</mark> x
-- law of associativity
forall f g h. (f >>= g) >>= h <mark>==</mark> f >>= (\x -> g x >>= h)
</code></pre>

---

## Functor/Applicative/Monad { data-transition="fade none" }

* So far we have seen data types that instance all three
* However
* Not all things that instance `Functor` also instance `Applicative`
* Not all things that instance `Applicative` also instance `Monad`

---

## Functor/Applicative/Monad

* What are some examples of data types that **cannot possibly** instance these type-classes?

---

## Functor/Applicative/Monad

###### is not a `Functor`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data FlipFunction a b = FlipFunction (b -> a)

-- exercise6
-- this will not be possible
-- try it to see why
instance Functor (FlipFunction a) where
</code></pre>

---

## Functor/Applicative/Monad

###### is a `Functor`, not `Applicative`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data FromIntOrBool a =
  FromInt (Int -> a)
  | FromBool (Bool -> a)

instance Functor FromIntOrBool where
  fmap f (FromInt g) =
    FromInt (f . g)
  fmap f (FromBool g) =
    FromBool (f . g)

-- exercise7
-- this will not be possible
-- try it to see why
instance Applicative FromIntOrBool where
</code></pre>

---

## Monad

###### derived operations

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- exercise8
flatten :: Monad k => k (k a) -> k a

-- exercise9
-- only when True return the given argument
onlyWhen :: Monad f => Bool -> f () -> f () 
</code></pre>

---

## Functor/Applicative/Monad { data-transition="zoom" }

<div style="font-size: xx-large; text-align: center">and a very important reminder</div>

---

## Functor/Applicative/Monad { data-transition="zoom" }

* why do we bother with these abstractions?
* what is the **practical purpose**?

---

## Functor/Applicative/Monad { data-transition="zoom" }

* The practical purpose here is to **minimise repetition of work**
* Furthermore, there is *no other practical purpose*

