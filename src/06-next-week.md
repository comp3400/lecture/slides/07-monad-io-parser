## Next week { data-transition="fade none" }

* There is no prac next week *(Good Friday)*
* Next week's lecture will have **no slides**
* It will be a **live coding session**

---

## Next week { data-transition="fade none" }

* Solving exercises from previous lectures
* If you want me to solve any specific problem, *please email me*
* Otherwise, I will just pick at random
* **Please bring your questions on how to solve problems**
* *(or email them to me prior)*

---


