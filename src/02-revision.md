## Revision { data-transition="fade none" }

###### Superclass/subclass relationships

<div style="text-align:center">
  <a href="images/superclass-subclass-labelled-01.png">
    <img src="images/superclass-subclass-labelled-01.png" alt="Superclass/subclass relationship" style="width:920px"/>
  </a>
</div>

---

## Revision { data-transition="fade none" }

###### For example

<div style="text-align:center">
  <a href="images/superclass-subclass-examples-02.png">
    <img src="images/superclass-subclass-examples-02.png" alt="Example of the superclass/subclass relationship" style="width:920px"/>
  </a>
</div>

---

## Revision { data-transition="fade none" }

###### Functor/Applicative type-class

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Functor k where
  fmap :: (a -> b) -> k a -> k b

class Functor k => Applicative k where
  (&lt;*>) :: k (a -> b) -> k a -> k b
  pure :: a -> k a
</code></pre>

---

## Revision { data-transition="fade none" }

###### Functor/Applicative instances and derived operations

<pre><code class="language-haskell hljs" data-trim data-noescape>
instance Functor Optional where ...
instance Functor List where ...
instance Functor ((->) t) where ...

instance Applicative Optional where ...
instance Applicative List where ...
instance Applicative ((->) t) where ...

flop :: Functor k => k (a -> b) -> a -> k b
anonmap :: Functor k => b -> k a -> k b
leftApply :: Applicative k => k b -> k a -> k b
rightApply :: Applicative k => k a -> k b -> k b
swizzle :: Applicative k => List (k x) -> k (List x)
</code></pre>

---

## Revision { data-transition="fade none" }

###### For example

<div style="text-align:center">
  <a href="images/superclass-subclass-examples-03.png">
    <img src="images/superclass-subclass-examples-03.png" alt="Example of the superclass/subclass relationship" style="width:920px"/>
  </a>
</div>

---

## Revision { data-transition="fade none" }

###### Observations

* What is an example data type that instances `Functor` but not instances `Applicative`?
* What is an example operation (function) that requires an `Applicative` constraint?
* *There are no data types that instance `Applicative` but not `Functor`*
* *There are no operations (functions) that require a `Functor` constraint, that wouldn't also work with an `Applicative` constraint*

---

## Revision

###### The general shape of Functor and Applicative

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmap ::    (a -> b) -> k a -> k b
(&lt;*>) :: k (a -> b) -> k a -> k b
</code></pre>

---

## Revision

###### The general shape of Functor/Applicative

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmap ::    (a -> b) -> k a -> k b
(&lt;*>) :: <mark>k</mark> (a -> b) -> k a -> k b
</code></pre>

---

## Revision

###### A general intuition for Functor/Applicative

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- Functor solves problems of this shape
(a -> b) -> k a -> k b
-- Applicative solves problems of this shape
(a -> b -> c) -> k a -> k b -> k c
</code></pre>

<div style="font-size: small; text-align: center">There are many ways to develop an intuition for these. This intuition is one way.</div>
<div style="font-size: small; font-weight: bold; text-align: center">PRACTICE</div>

