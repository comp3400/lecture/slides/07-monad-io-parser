## A closer look at `IO` { data-transition="fade none" }

---

## A closer look at `IO` { data-transition="fade none" }

<pre><code data-trim data-noescape>
> :info IO
newtype IO a = ...
instance Functor IO
instance Applicative IO
instance Monad IO
</code></pre>

---

## A closer look at `IO` { data-transition="fade none" }

###### Therefore

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmap :: (a -> b) -> IO a -> IO b
(&lt;*>) :: IO (a -> b) -> IO a -> IO b
(=&lt;&lt;) :: (a -> IO b) -> IO a -> IO b
(>>=) :: IO a -> (a -> IO b) -> IO b
-- also applies to derived operations
</code></pre>

---

## A closer look at `IO` { data-transition="fade none" }

###### Some built-in functions

<pre><code class="language-haskell hljs" data-trim data-noescape>
readFile :: FilePath -> IO String
writeFile :: FilePath -> String -> IO ()
getChar :: IO Char
getLine :: IO String
putStrLn :: String -> IO ()
print :: Show a => a -> IO ()
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmap :: (a -> b) -> IO a -> IO b
(&lt;*>) :: IO (a -> b) -> IO a -> IO b
(=&lt;&lt;) :: (a -> IO b) -> IO a -> IO b
(>>=) :: IO a -> (a -> IO b) -> IO b
-- also applies to derived operations
</code></pre>

---

## A use-case with `IO` { data-transition="fade none" }

* Have the user enter two filenames, `f1` and `f2`
* Read each filename
* With the contents of `f1`, turn it all to upper-case
* With the contents of `f2`, reverse it
* Append the two results
  * Print to stdout
  * Write to a filename that is passed as an argument

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-trim data-noescape>
-- my notes

getLine :: IO String
-- Have the user enter two filenames, f1 and f2

readFile :: FilePath -> IO String
-- Read each filename

toUpper :: Char -> Char
fmap toUpper :: String -> String
-- toUpper is in module Data.Char
-- With the contents of f1, turn it all to upper-case

reverse :: [a] -> [a]
-- With the contents of f2, reverse it

(++) :: [a] -> [a] -> [a]
-- Append the two results

putStrLn :: String -> IO ()
-- Print to stdout

writeFile :: FilePath -> String -> IO ()
-- Write to a filename that is passed as an argument
</code></pre>
</div>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  ...











</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
getLine :: IO String
(>>=) :: IO a -> (a -> IO b) -> IO b




</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  <mark>getLine</mark> >>= \f1 ->
  ...











</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
getLine :: <mark>IO String</mark>
(>>=) :: <mark>IO a</mark> -> (a -> IO b) -> IO b




</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= <mark>\f1 -></mark>
  <mark>...</mark>











</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
getLine :: IO String
(>>=) :: IO a -> <mark>(a -> IO b)</mark> -> IO b




</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  <mark>...</mark>











</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
getLine :: IO String
(>>=) :: IO a -> (a -> <mark>IO b</mark>) -> <mark>IO b</mark>




</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  <mark>getLine</mark> >>= \f2 ->
  ...</mark>










</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
getLine :: <mark>IO String</mark>
(>>=) :: IO a -> (a -> IO b) -> IO b
f1 :: String



</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= <mark>\f2 -></mark>
  <mark>...</mark>










</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
getLine :: IO String
(>>=) :: IO a -> <mark>(a -> IO b)</mark> -> IO b
f1 :: String



</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  readFile f1 >>= \contents1 ->
  ...








</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
readFile :: FilePath -> IO String
(>>=) :: IO a -> (a -> IO b) -> IO b
f1 :: String
f2 :: String


</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  <mark>readFile f1</mark> >>= \contents1 ->
  ...








</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
readFile :: FilePath -> <mark>IO String</mark>
(>>=) :: <mark>IO a</mark> -> (a -> IO b) -> IO b
f1 :: String
f2 :: String
contents1 :: String

</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  ...







</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
readFile :: FilePath -> IO String
(>>=) :: IO a -> (a -> IO b) -> IO b
f1 :: String
f2 :: String
contents1 :: String
contents2 :: String
</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  {- With the contents of f1, turn it all to upper-case
     With the contents of f2, reverse it
     Append the two results -}
  let result = (fmap toUpper contents1) ++ (reverse contents2)
  in  ...



</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
f1 :: String
f2 :: String
contents1 :: String
contents2 :: String
result :: String

</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  {- With the contents of f1, turn it all to upper-case
     With the contents of f2, reverse it
     Append the two results -}
  let result = (fmap toUpper contents1) ++ (reverse contents2)
      {- Print to stdout -}
  in  putStrLn result


</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
putStrLn :: String -> IO ()
result :: String




</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  {- With the contents of f1, turn it all to upper-case
     With the contents of f2, reverse it
     Append the two results -}
  let result = (fmap toUpper contents1) ++ (reverse contents2)
      {- Print to stdout -}
  in  <mark>putStrLn</mark> result


</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
<mark>putStrLn</mark> :: String -> IO ()
result :: String




</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  {- With the contents of f1, turn it all to upper-case
     With the contents of f2, reverse it
     Append the two results -}
  let result = (fmap toUpper contents1) ++ (reverse contents2)
      {- Print to stdout -}
  in  putStrLn <mark>result</mark>


</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
putStrLn :: <mark>String</mark> -> IO ()
result :: <mark>String</mark>




</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  {- With the contents of f1, turn it all to upper-case
     With the contents of f2, reverse it
     Append the two results -}
  let result = (fmap toUpper contents1) ++ (reverse contents2)
      {- Print to stdout -}
  in  <mark>putStrLn result</mark>


</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
(>>=) :: <mark>IO a</mark> -> (a -> IO b) -> IO b
putStrLn :: String -> <mark>IO ()</mark>
result :: String



</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  {- With the contents of f1, turn it all to upper-case
     With the contents of f2, reverse it
     Append the two results -}
  let result = (fmap toUpper contents1) ++ (reverse contents2)
      {- Print to stdout -}
  in  putStrLn result >>= \_ ->
      ...

</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
(>>=) :: IO a -> (a -> IO b) -> IO b
putStrLn :: String -> IO ()
result :: String



</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> IO ()
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  {- With the contents of f1, turn it all to upper-case
     With the contents of f2, reverse it
     Append the two results -}
  let result = (fmap toUpper contents1) ++ (reverse contents2)
      {- Print to stdout -}
  in  putStrLn result >>= \_ ->
      {- Write to a filename that is passed as an argument -}
      writeFile outfile result
</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
(>>=) :: IO a -> (a -> IO b) -> IO b
putStrLn :: String -> IO ()
writeFile :: FilePath -> String -> IO ()
result :: String


</code></pre>

---

## A use-case with `IO`

<div style="font-size: 32px">
<pre><code class="language-haskell hljs" data-noescape data-line-numbers>myfunction :: FilePath -> <mark>IO ()</mark>
myfunction outfile =
  {- Have the user enter two filenames, f1 and f2 -}
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  {- Read each filename -}
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  {- With the contents of f1, turn it all to upper-case
     With the contents of f2, reverse it
     Append the two results -}
  let result = (fmap toUpper contents1) ++ (reverse contents2)
      {- Print to stdout -}
  in  putStrLn result >>= \_ ->
      {- Write to a filename that is passed as an argument -}
      <mark>writeFile outfile result</mark>
</code></pre>
</div>

<pre><code class="language-haskell hljs" data-noescape>-- notes
(>>=) :: IO a -> (a -> <mark>IO b</mark>) -> <mark>IO b</mark>
putStrLn :: String -> IO ()
writeFile :: FilePath -> String -> <mark>IO ()</mark>
result :: String


</code></pre>

---

## A solution for the use-case with `IO`

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  let result =
        (fmap toUpper contents1) ++ (reverse contents2)
  in  putStrLn result >>= \_ ->
      writeFile outfile result
</code></pre>

---

## A solution for the use-case with `IO`

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  let result =
        (fmap toUpper contents1) ++ (reverse contents2)
  in  putStrLn result >>= \_ ->
      writeFile outfile result
</code></pre>

* This convenient laying out of code is why `(>>=)` is sometimes preferred over `(=<<)`
* Using `(=<<)` would have also required the addition of parentheses *(try it!)*
* Also ...

---

## `do` notation { data-transition="fade none" }

###### One more bit of syntax!

---

## `do` notation 

* Haskell has a `do` keyword
* The `do` keyword provides special syntax that translates directly *at compile-time* to `(>>=)`

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
      getLine >>= \f1 ->
      getLine >>= \f2 ->
      readFile f1 >>= \contents1 ->
      readFile f2 >>= \contents2 ->
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  putStrLn result >>= \_ ->
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. turn `>>=` into `<-`
4. delete `->`
5. delete `\`
6. swap each side of `<-`
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  <mark>do</mark>  getLine >>= \f1 ->
      getLine >>= \f2 ->
      readFile f1 >>= \contents1 ->
      readFile f2 >>= \contents2 ->
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  putStrLn result >>= \_ ->
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. <mark>insert the keyword `do`</mark>
2. <mark>apply appropriate spacing</mark>
3. turn `>>=` into `<-`
4. delete `->`
5. delete `\`
6. swap each side of `<-`
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  getLine <mark>>>=</mark> \f1 ->
      getLine <mark>>>=</mark> \f2 ->
      readFile f1 <mark>>>=</mark> \contents1 ->
      readFile f2 <mark>>>=</mark> \contents2 ->
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  putStrLn result <mark>>>=</mark> \_ ->
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. <mark>turn `>>=` into `<-`</mark>
4. delete `->`
5. delete `\`
6. swap each side of `<-`
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  getLine <mark>&lt;-</mark> \f1 ->
      getLine <mark>&lt;-</mark> \f2 ->
      readFile f1 <mark>&lt;-</mark> \contents1 ->
      readFile f2 <mark>&lt;-</mark> \contents2 ->
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  putStrLn result <mark>&lt;-</mark> \_ ->
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. <mark>turn `>>=` into `<-`</mark>
4. delete `->`
5. delete `\`
6. swap each side of `<-`
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  getLine &lt;- \f1 <mark>-></mark>
      getLine &lt;- \f2 <mark>-></mark>
      readFile f1 &lt;- \contents1 <mark>-></mark>
      readFile f2 &lt;- \contents2 <mark>-></mark>
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  putStrLn result &lt;- \_ <mark>-></mark>
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. turn `>>=` into `<-`
4. <mark>delete `->`</mark>
5. delete `\`
6. swap each side of `<-`
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  getLine &lt;- \f1 
      getLine &lt;- \f2
      readFile f1 &lt;- \contents1
      readFile f2 &lt;- \contents2
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  putStrLn result &lt;- \_
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. turn `>>=` into `<-`
4. <mark>delete `->`</mark>
5. delete `\`
6. swap each side of `<-`
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  getLine &lt;- <mark>\</mark>f1
      getLine &lt;- <mark>\</mark>f2
      readFile f1 &lt;- <mark>\</mark>contents1
      readFile f2 &lt;- <mark>\</mark>contents2
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  putStrLn result &lt;- <mark>\</mark>_
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. turn `>>=` into `<-`
4. delete `->`
5. <mark>delete `\`</mark>
6. swap each side of `<-`
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  getLine &lt;-  f1
      getLine &lt;-  f2
      readFile f1 &lt;-  contents1
      readFile f2 &lt;-  contents2
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  putStrLn result &lt;-  _
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. turn `>>=` into `<-`
4. delete `->`
5. <mark>delete `\`</mark>
6. swap each side of `<-`
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  getLine &lt;-  f1
      getLine &lt;-  f2
      readFile f1 &lt;-  contents1
      readFile f2 &lt;-  contents2
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  putStrLn result &lt;-  _
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. turn `>>=` into `<-`
4. delete `->`
5. delete `\`
6. <mark>swap each side of `<-`</mark>
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  <mark>getLine</mark> &lt;-  <mark>f1</mark>
      <mark>getLine</mark> &lt;-  <mark>f2</mark>
      <mark>readFile f1</mark> &lt;-  <mark>contents1</mark>
      <mark>readFile f2</mark> &lt;-  <mark>contents2</mark>
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  <mark>putStrLn result</mark> &lt;-  <mark>_</mark>
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. turn `>>=` into `<-`
4. delete `->`
5. delete `\`
6. <mark>swap each side of `<-`</mark>
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  <mark>f1</mark> &lt;- <mark>getLine</mark>
      <mark>f2</mark> &lt;- <mark>getLine</mark>
      <mark>contents1</mark> &lt;- <mark>readFile f1</mark>
      <mark>contents2</mark> &lt;- <mark>readFile f2</mark>
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      in  <mark>_</mark> &lt;- <mark>putStrLn result</mark>
          writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. turn `>>=` into `<-`
4. delete `->`
5. delete `\`
6. <mark>swap each side of `<-`</mark>
7. delete the `in` keyword in any `let/in` expression and space appropriately

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  f1 &lt;- getLine
      f2 &lt;- getLine
      contents1 &lt;- readFile f1
      contents2 &lt;- readFile f2
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      <mark>in  </mark>_ &lt;- putStrLn result
      <mark>    </mark>writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. turn `>>=` into `<-`
4. delete `->`
5. delete `\`
6. swap each side of `<-`
7. <mark>delete the `in` keyword in any `let/in` expression and space appropriately</mark>

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  f1 &lt;- getLine
      f2 &lt;- getLine
      contents1 &lt;- readFile f1
      contents2 &lt;- readFile f2
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      _ &lt;- putStrLn result
      writeFile outfile result
</code></pre>

<div style="font-size: small; font-weight:bold; text-align: center">Follow these steps</div>

1. insert the keyword `do`
2. apply appropriate spacing
3. turn `>>=` into `<-`
4. delete `->`
5. delete `\`
6. swap each side of `<-`
7. <mark>delete the `in` keyword in any `let/in` expression and space appropriately</mark>

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  f1 &lt;- getLine
      f2 &lt;- getLine
      contents1 &lt;- readFile f1
      contents2 &lt;- readFile f2
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      _ &lt;- putStrLn result
      writeFile outfile result
</code></pre>

<div style="font-size: x-large; text-align: center">This is **the same program** with only syntactic differences</div>

---

## `do` notation

<div style="font-size: 36px">
<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  getLine >>= \f1 ->
  getLine >>= \f2 ->
  readFile f1 >>= \contents1 ->
  readFile f2 >>= \contents2 ->
  let result =
        (fmap toUpper contents1) ++ (reverse contents2)
  in  putStrLn result >>= \_ ->
      writeFile outfile result
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  f1 &lt;- getLine
      f2 &lt;- getLine
      contents1 &lt;- readFile f1
      contents2 &lt;- readFile f2
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      _ &lt;- putStrLn result
      writeFile outfile result
</code></pre>
</div>

---

## To `do` or not to `do`

* This will work for *any* `Monad`, not just `IO`
* This course does not specify a preference for one or the other
* Use the one that *best assists **you** in getting to a solution*

---

## `do` notation

<pre><code class="language-haskell hljs" data-trim data-noescape data-line-numbers>
myfunction :: FilePath -> IO ()
myfunction outfile =
  do  f1 &lt;- getLine
      f2 &lt;- getLine
      contents1 &lt;- readFile f1
      contents2 &lt;- readFile f2
      let result =
            (fmap toUpper contents1) ++
            (reverse contents2)
      _ &lt;- putStrLn result
      writeFile outfile result
</code></pre>

<div style="font-size: x-large; text-align: center">Looks a bit like C or Java, doesn't it?</div>
<div style="font-size: xx-small; text-align: center">(and a bit neater)</div>

